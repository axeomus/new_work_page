const { src,dest,watch }    = require('gulp'),
      { series,parallel}    = require('gulp'),
        gulpif              = require("gulp-if"),
        del                 = require("del"),
        concat              = require("gulp-concat"),
        htmlmin             = require("gulp-htmlmin"),
        sass                = require("gulp-sass"),
        cssnano             = require("gulp-cssnano"),
        autoprefixer        = require("gulp-autoprefixer"),
        uglify              = require('gulp-uglify'),
        zip                 = require("gulp-zip"),
        browserSync         = require("browser-sync").create();

// Пути
const path = {
    dest: {
        all: 'dest/**/*',
        root: 'dest/',
        img: 'dest/img/',
        css: 'dest/css/',
        js: 'dest/js/',
        fonts: 'dest/fonts/'
    },
    src: {
        all: 'src/**/*',
        root: 'src/',
        html: 'src/**/*.html',
        img: 'src/img/**/*.+(jpg|jpeg|png|gif|svg)',
        scss: 'src/scss/style.scss',
        css: 'src/css/**/*.css',
        js: 'src/js/**/*.js',
        fonts: 'src/fonts/**/*'
    },
    watch: {
        scss: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        html: 'src/**/*.html',
        img: 'src/img/**/*.+(jpg|jpeg|png|gif|svg)'
    }
}

// Задача абота с HTML

function html() {
    return src(path.src.html)
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(dest(path.dest.root))
}

// Задача SCSS in CSS

function scss() {
    return src(path.src.scss)
        .pipe(sass())
        .pipe(dest('src/css/'))
}

// Задача сжать CSS

function mincss() {
    return src(path.src.css)
        .pipe(autoprefixer({
            browsers: ['last 1 version'],
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(dest(path.dest.css))
}

// Задача сжать JS

function minjs() {
    return src(path.src.js)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(dest(path.dest.js))
}

// Задача перемещает папку с шрифтами

function fonts() {
    return src(path.src.fonts)
        .pipe(dest(path.dest.fonts));
}

//Задача перемещения картинок

function img() {
    return src(path.src.img)
        .pipe(dest(path.dest.img))
}


function clean() {
    return del([path.dest.root])
}


function ziper() {
    return src(path.src.all)
        .pipe(zip('src.zip'))
        .pipe(dest(path.dest.root))
}

// Задача создать рабочий сервер

function BrowserSync() {
    browserSync.init({
        server: {
            baseDir: 'src'
        }
    })
}

function reload(done) {
    browserSync.reload();
    done();
}

// Задача начать отслеживать файлы

function watchDev() {
    watch(path.watch.scss, series(scss, reload));
    watch(path.watch.js, reload);
    watch(path.watch.html, reload);
    watch(path.watch.img, reload);
}

// Задачи запуска через gulp [task]

exports.html = html;
exports.scss = scss;
exports.mincss = mincss;
exports.minjs = minjs;
exports.img = img;
exports.fonts = fonts;
exports.clean = clean;
exports.zip = ziper;
exports.live = series(
    scss,
    parallel(BrowserSync, watchDev)
);

//Создание проекта без сжатия img
exports.build = series(
    clean,
    parallel(
        html,
        series(scss, mincss),
        minjs,
        img,
        fonts
    ),
    ziper
);