(function () {
    let body = document.querySelector('body');
    body.innerHTML += '<div id="pixp" style="background: white;position: fixed; bottom: 10px; right: 10px; z-index: 1000;height: 200px !important;min-height: 200px !important; width: 250px;"><form><input type="text" name="src" style="width: 150px"><input type="button" name="load" value="load"><br>Opacity: <input type="number" name="opacity" min="0" max="1" step="0.1" value="1"><br>Margin: <input type="number" name="margin" value="0" step="1"><br><input type="button" name="save" value="save"></form></div>';
    body.innerHTML += '<img id="piximg" src="" style="position: absolute; top: 0; left:0; max-width: 100%; ">';
    let pp = document.getElementById('pixp');
    let piximg = document.getElementById('piximg');
    let inputSrc = document.querySelector("input[name=src]");
    let inputLoad = document.querySelector("input[name=load]")
    let pixOp = document.querySelector("input[name=opacity]");
    let pixMar = document.querySelector("input[name=margin]");
    if (localStorage.src || localStorage.opacity || localStorage.margin) {
        inputSrc.value = localStorage.src;
        pixOp.value = localStorage.opacity;
        pixMar.value = localStorage.margin;
    }
    pp.onclick = function (event) {
        switch (event.target.name) {
            case "load":
                piximg.src = 'psd/' + inputSrc.value + '.png';
                break;
            case "margin":
                piximg.style.left = event.target.value + 'px';
                break;
            case "opacity":
                piximg.style.opacity = pixOp.value;
                break;
            case "save":
                localStorage.src = inputSrc.value;
                localStorage.opacity = pixOp.value;
                localStorage.margin = pixMar.value;
                break;
        }
    };

    (function clickLoad() {
        inputLoad.click();
        pixOp.click();
        pixMar.click();
    })();

    pp.onchange = function (event) {
        if (event.target.name == 'margin') {
            console.log(event.target.name);
            piximg.style.left = event.target.value + 'px';
        }
        if (event.target.name == 'opacity') {
            piximg.style.opacity = pixOp.value;
        }
    }
})();
