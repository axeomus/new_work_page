(function () {
    var accordionContents = $('#accordion .card');
    var sliderLink = $('.carousel-links__item');

    // Slider function's

    sliderLink.on('click', function (e) {
        sliderLink.filter('.active').toggleClass('active');
        $(this).toggleClass('active');
    })

    // Main acordion function's
    $('.accordion__btn').on('click', function () {
        var $this = $(this);
        var $parent = $this.parents('.card');

        if($this.parents('.card').hasClass('active')) {
            $parent.toggleClass('active');
            return true;
        }
        accordionContents.filter('.active').toggleClass('active');
        $parent.toggleClass('active');
    })
})();